# Changelog
All notable changes to this project will be documented in this file.

## [1.4.0] - 2020-06-03

### Changed
- Bucket configuration now stored in a JSON file

## [1.3.4] - 2020-06-01

### Added
- A CSS style overrides file

### Changed
- Rounded the corners of the last item in the file list

## [1.3.3] - 2020-05-31

### Added
- Ability to scroll images in the image gallery with up/down arrow keys

### Changed
- Default to dark mode
- Enabled 'use strict'
- Moved from using 'var' to 'let'

## [1.3.2] - 2020-04-28
### Added
- Tooltip to the image gallery close button (you can press Esc to close it)

### Changed
- Fixed dark mode bug where the image gallery close button was difficult to see
- Natural sort order for files (#4)

## [1.3.1] - 2020-04-28
### Changed
- Fixed bug where some files would not embed due to having spaces in the name

## [1.3.0] - 2020-04-28
### Added
- Image gallery (#3)

### Changed
- Fixed bug where exiting dark mode did not revert everything until refresh
- Moved getFileIcon to a different file (#2)

## [1.2.0] - 2020-04-27
### Added
- Image preview embed (#1)

### Changed
- Moved the actual code to the `src` folder
- Updated the readme with more complete information

## [1.1.2] - 2019-08-13
### Changed
- Minor formatting improvements in UI tweaks JS file
- Added FontAwesome CSS to index HTML file

## [1.1.1] - 2019-08-13
### Added
- Icon change when expanding/collapsing a folder

## [1.1.0] - 2019-08-13
### Added
- Collapsing/expanding folder views
- FontAwesome icons for files based on extension

## [1.0.2] - 2019-01-17
### Changed
- Updated Bootstrap
- Now referencing local Bootstrap and jQuery libraries because neither
  Bootstrap nor jQuery's CDNs support IPv6...

## [1.0.1] - 2018-07-24
### Changed
- Redid readme
- Renamed changelog

## [1.0.0] - 2018-07-24
### Added
- Changelog

### Changed
- Fixed issues where refreshing after having clicked an anchor link resulted
  in each link being messed up (e.g. domain.tld/#about/favicon.ico instead of
  domain.tld/favicon.ico) and unclickable as a result.
