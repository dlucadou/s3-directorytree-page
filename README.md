# S3 Directory Listing

A lightweight webpage that you can put in an S3 bucket to act as a file listing. It also has a dark mode that saves its state via a cookie. It works with CloudFront distributions (and presmuably with any other CDN that can go in front of an S3 bucket), and it shows file additions/deletions immediately since it goes to S3 and does not depend on cache invalidations in a CDN.  
It also has image previews - you can expand images and embed them, or open the gallery viewer I wrote myself and use buttons or arrow keys to navigate through the pictures.

You can view a live version of this [here](https://files.lucadou.sh/).

## Getting started

### Prerequisites

The client will need to be able to download Bootstrap and jQuery, and they must have JavaScript enabled.  
The bucket must be specifically configured, instructions for which are provided below.

### Installation and Deployment

1. Open [config.json](src/assets/js/config.json) and set the `region` and `bucket` variables. Only modify `startingPosition` if you know what you are doing.
2. Open [style-overrides.css](src/assets/css/style-overrides.css) and add any CSS modifications you want.
3. Update [index.html](src/index.html) with any custom attributes or elements you want.
4. Place in an S3 bucket (can be the root or a subfolder, but it will display all files in the bucket).
5. Enable static website hosting on the bucket.
6. Ensure the bucket is publically accessible.
7. Enable CORS through either a specific configuration to only allow enumeration from your bucket:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<CORSConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
    <CORSRule>
    <AllowedOrigin>https://your.fqdn.here</AllowedOrigin>
    <AllowedOrigin>https://your-bucket-name.s3.amazonaws.com</AllowedOrigin>
    <AllowedOrigin>https://your-bucket-name.s3.your-s3-region.amazonaws.com</AllowedOrigin>
    <AllowedOrigin>https://s3.your-s3-region.amazonaws.com/your-bucket-name/*</AllowedOrigin>
    <AllowedOrigin>https://s3.amazonaws.com/your-bucket-name/*</AllowedOrigin>
    <AllowedMethod>GET</AllowedMethod>
    <MaxAgeSeconds>3000</MaxAgeSeconds>
    <AllowedHeader>Authorization</AllowedHeader>
</CORSRule>
</CORSConfiguration>
```

Make sure to replace `your-bucket-name` with your bucket's name, and `your-s3-region` with the region your bucket is in.  
If your bucket is *not* in a legacy global endpoint region, remove remove the global endpoint lines (that is, the 2 lines that have `AllowedOrigin` for `s3.amazonaws.com`). For more information about the legacy global endpoint, see [this documentation](https://docs.aws.amazon.com/AmazonS3/latest/dev/VirtualHosting.html#VirtualHostingBackwardsCompatibility).  
Or you can allow anyone to list your bucket's contents:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<CORSConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
<CORSRule>
    <AllowedOrigin>*</AllowedOrigin>
    <AllowedMethod>GET</AllowedMethod>
    <MaxAgeSeconds>3000</MaxAgeSeconds>
    <AllowedHeader>Authorization</AllowedHeader>
</CORSRule>
</CORSConfiguration>
```

## Versioning

We use Semantic Versioning, aka [SemVer](https://semver.org/spec/v2.0.0.html).

## Authors

* Luna Lucadou - Adapted original source to Bootstrap and worked out some flaws

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE.md](LICENSE.md) file for details.

## Acknowledgements

* Prabha Sharma, whose [initial work](https://github.com/prabhatsharma/s3-directorylisting) is what I based this off of.
