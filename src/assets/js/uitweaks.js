'use strict';
function detectIE() {
  // Source https://stackoverflow.com/a/29141430
  let ua = window.navigator.userAgent;
  let msie = ua.indexOf('MSIE ');
  let trident = ua.indexOf('Trident/');
  let edge = ua.indexOf('Edge/');
  if (msie > 0) {
    // IE 10 or older 
    return true;
  }
  else if (trident > 0) {
    // IE 11 
    return true;
  }
  else {
    // All other browsers, including Edge
    return false;
  }
}

// Source for the cookie methods:
// https://stackoverflow.com/questions/14573223/set-cookie-and-get-cookie-with-javascript
function setCookie(name,value,days) {
  let expires = "";
  if (days) {
    let date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    expires = "; expires=" + date.toUTCString();
  }
  document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function getCookie(name) {
  let nameEQ = name + "=";
  let ca = document.cookie.split(';');
  for(let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0)==' ') {
      c = c.substring(1,c.length);
    }
    if (c.indexOf(nameEQ) == 0) {
      return c.substring(nameEQ.length,c.length);
    }
  }
  return null;
}

function eraseCookie(name) {   
  document.cookie = name+'=; Max-Age=-99999999;';  
}

function changeLogoOnDropdown(ddToggles) {
  /* Note that this does mess up if a person spams the toggle,
   * but considering what a mess trying to work around it would be
   * (and according to my research, there is no way to query the collapsed
   * /non-collapsed state of Boostrap navbars), I'm just leaving it as-is.
   */
  logo = document.getElementById('navbar-logo');
  if (ddToggles % 2 == 0) {
    logo.innerHTML = '<img src="/img/logos/logo-dropdown.png" class="navbar-logo" style="width: 45px;">';
  } else {
    setTimeout(function() {
      logo.innerHTML = '<img src="/img/logos/logo-main.png" style="width: 40px;">';
    }, 150); // Wait 150 milliseconds before running ^ so it doesn't cover text during the close menu animation
  }
}

function lightMode(lmToggles) {
  let b = document.getElementById('body');
  if (lmToggles % 2 != 0) { // Light mode was enabled, being disabled
    eraseCookie('lightMode');
    b.classList.remove('body-dark');
    $('#gallery-modal-content').removeClass('bg-dark');
    $('#galleryModalClose').removeClass('text-white');
    $('#files').find('*').removeClass('bg-dark');
    $('#files').find('*').removeClass('text-white');
  } else { // Light mode was disabled, being enabled
    setCookie('lightMode', 'on');
    b.classList.add('body-dark');
    $('#gallery-modal-content').addClass('bg-dark');
    $('#galleryModalClose').addClass('text-white');
    $('#files').find('*').addClass('bg-dark');
    $('#files').find('*').addClass('text-white');
  }
}

/* Takes 2 Strings and returns which comes first alphabetically, naturally.
 * Works with numbers both at the start and end!
 * Many natural sorting algorithms will work only with numbers at the start
 * (e.g. [1a, 10a, 2a, 20a, a1, a10, a2, a20] =>
 *         [1a, 2a, 10a, 20a, a1, a10, a2, a20])
 * This algorithm, however, works with numbers at either end.
 * (e.g. [1a, 10a, 2a, 20a, a1, a10, a2, a20] =>
 *         [1a, 2a, 10a, 20a, a1, a2, a10, a20])
 * I tried localeCompare and could not get it to work, so I had to use this.
 * https://stackoverflow.com/a/15479354
 */
function naturalCompare(a, b) {
  let ax = [], bx = [];

  a.replace(/(\d+)|(\D+)/g, function(_, $1, $2) { ax.push([$1 || Infinity, $2 || ""]) });
  b.replace(/(\d+)|(\D+)/g, function(_, $1, $2) { bx.push([$1 || Infinity, $2 || ""]) });

  while(ax.length && bx.length) {
    let an = ax.shift();
    let bn = bx.shift();
    let nn = (an[0] - bn[0]) || an[1].localeCompare(bn[1]);
    if(nn) return nn;
  }

  return ax.length - bx.length;
}

/* Takes 2 S3 keys and returns which comes first alphabetically, naturally.
 * Works with numbers at both the start and end.
 */
function naturalNodeComparator(node1, node2){
  // Extract node text
  let as = node1.getElementsByTagName('Key')[0].textContent;
  let bs = node2.getElementsByTagName('Key')[0].textContent;
  return naturalCompare(as, bs);
}

function getFolderIcon(isClosed, htmlTag = "i", paddingClass = "pr-1") {
  /* isClosed - boolean.
   * htmlTag - the tag you want the element to use, e.g. "span", "div" (defaults to "i").
   * paddingClass - the class to use for padding the icon; defaults to Bootstrap's "pr-1". (Can also be used to add additional classes.)
   */
  if (isClosed) {
    return '<{0} class="fas fa-folder {1}"></{0}>'.split('{0}').join(htmlTag).split('{1}').join(paddingClass);
  } else {
    return '<{0} class="fas fa-folder-open {1}"></{0}>'.split('{0}').join(htmlTag).split('{1}').join(paddingClass);
  }
}

function checkForFolderNode(filePathArray) {
  /* filePathArray - an array containing a directory path.
   * Should not contain the file name, just the enclosing folder structure.
   * e.g. path/to/file.txt should be given as ['path', 'to'].
   */
  let folderNode = 'files-dir-' + filePathArray.join('-');
  return document.getElementById(folderNode) !== null;
}

function createFolderNodes(filePath) {
  let pathArray = filePath.split("/");
  let folderArray = pathArray.slice(0, pathArray.length - 1);

  // Create the necessary folder nodes
  for (let i = 0; i < pathArray.length; i++) {
    // Start at 1 because folderArray.slice(0, 0) will always return [].
    if (!checkForFolderNode(folderArray.slice(0, i+1))) {
      let darkModeClassList = 'bg-dark text-white';
      let folderPath = folderArray.slice(0, i+1).join('-');
      let folderElement = '<li class="list-group-item d-flex justify-content-between align-items-center {-1}"><a data-toggle="collapse" class="{-1}" href="#files-dir-{}" role="button" aria-expanded="false" aria-controls="files-dir-{}" class="folder-list-entry {-1}">{0}{1}</a></li><div class="collapse {-1}" id="files-dir-{}"><div class="col-1 {-1}"></div><div class="col {-1}"><div class="list-group-item justify-content-between align-items-center p-0 border-0 {-1}" id="files-dir-{}"><ul class="list-group {-1}" id="folder-list-{}"></ul><ul class="list-group {-1}" id="file-list-{}"></ul></div></div></div>'.split('{}').join(folderPath).split('{0}').join(getFolderIcon(true)).split('{1}').join(folderArray[i]);
      if (!getCookie('lightMode')) { // Dark mode enabled
        folderElement = folderElement.split('{-1}').join(darkModeClassList);
      } else {
        folderElement = folderElement.split('{-1}').join('');
      }
      if (i == 0) { // Folder is in root directory
        $('#file-list').append(folderElement);
      } else { // Folder is not in root directory
        let parentFolderPath = 'folder-list-' + folderArray.slice(0, i).join('-');
          /* I have a separate file-list-* and folder-list-* so I can ensure
           * folders will always come before files in the list.
           */
        $('#' + parentFolderPath).append(folderElement);
      }
    }
  }
}

function createFileListing(filePath, url, fileCount) {
  /* filePath - the path up to and including the filename, e.g. path/to/file.txt
   * url - the complete URL where the file can be accessed, e.g. https://example.com/path/to/file.txt
   */
  let pathArray = filePath.split("/");
  let folderArray = pathArray.slice(0, pathArray.length - 1);
  let fileName = pathArray[pathArray.length - 1];
  let fileNameNoExt = fileName.split('.')[0];
    /* If there is no '.', the string will be unaffected, as split will return
     * a 1-element array. It will not throw an exception.
     */
  let fileElementID = 'file-a-{}'.split('{}').join(fileCount);
    // Give each element a unique ID.

  let darkModeClassList = 'bg-dark text-white';
  let fileElementBasicTemplate = '<li class="list-group-item {-1}"><a href="{2}" id="{1}">{3}{4}</a></li>';
  let fileElementImgTemplate = '<li class="list-group-item {-1}"><a href="{2}" id="{1}" target="_blank">{3}{4} <i class="fas fa-external-link-alt"></i></a>{0}</li>';
  let imgEmbedTemplate = '<span class="ml-2 mr-2 {-1}">|</span><a class="accordion-row-toggler {-1}" role="button" data-toggle="collapse" data-target="#preview-img-collapse-{5}" href="{2}" aria-expanded="true" aira-controls="img-collapse-{5}">Embed <i class="fas fa-chevron-circle-down {-1}"></i></a><span class="ml-2 mr-2 {-1}">|</span><a href="#" type="button" class="button-open-gallery {-1} void" data-toggle="modal" data-target="#galleryModal">Open in Gallery View <i class="fas fa-images {-1}"></i></a><span class="collapse {-1}" id="preview-img-collapse-{5}" style=""><ul class="list-group {-1}" id="file-list-hosted-img-chat-{5}"><li class="list-group item {-1}"><label>File - {4}</label><img src="{2}" loading="lazy"></li></ul></span>';
  createFolderNodes(filePath);
  
  // Create the file listing
  let fileElement;
  if (getFileIcon(fileName).indexOf('fa-file-image') > 0) {
    fileElement  = fileElementImgTemplate;
  } else {
    fileElement  = fileElementBasicTemplate;
  }
  if (!getCookie('lightMode')) { // Dark mode enabled
    fileElement = fileElement.split('{-1}').join(darkModeClassList);
  } else {
    fileElement = fileElement.split('{-1}').join('');
  }
  fileElement = fileElement.split('{0}').join(imgEmbedTemplate);
  fileElement = fileElement.split('{1}').join(fileElementID);
  fileElement = fileElement.split('{2}').join(url);
  fileElement = fileElement.split('{3}').join(getFileIcon(fileName));
  fileElement = fileElement.split('{4}').join(fileName);
  fileElement = fileElement.split('{5}').join(fileCount);
    /* Using .split and .join is easier than writing a .replaceAll method or
     * using regex in .replace.
     * If the element to split on is not present, it returns a 1-element array,
     * so the .join will not affect it or throw an error if '{3}' or '{4}' are
     * not present (i.e. if the link is not an image).
     */

  if (pathArray.length == 1) { // File is in root directory
    $('#file-list').append(fileElement);
  } else { // File is not in root directory
    let folderFileNode = 'file-list-' + folderArray.join('-');
    $('#' + folderFileNode).append(fileElement);
  }
}

function fileList(region, bucket, startingPosition) {
  let startPosition = bucket.search(startingPosition);
  let domain;
  if (region === 'us-east-1') {
    domain = 's3.amazonaws.com';
  } else {
    domain = 's3-' + region + '.amazonaws.com';
  }

  let url = 'https://' + domain + "/" + bucket + "/";
  let fileCount = 0;
  fetch(url).then(res => {
    let sMyString = res.text().then(text => {
      let oParser = new DOMParser();
      let oDOM = oParser.parseFromString(text, "text/xml");
      
      oDOM.childNodes.forEach((value, index, listObj) => {
        Array.from(value.childNodes)
            .filter(element => element.localName === 'Contents')
              /* Non-Contents elements do not have the value the comparator
               * looks for, so not filtering them out would cause exceptions.
               */
            .sort(naturalNodeComparator)
            .forEach((value1, index1, listObj1) => {
          value1.childNodes.forEach((value2, index2, listObj2) => {
            if (value2.localName == 'Key' && value2.textContent != 'index.html') {
              let fileName = value2.textContent;
              let url = "https://" + window.location.hostname + "/" + fileName;
              let pathArray = fileName.split("/");
              // Adapted from https://stackoverflow.com/a/36249526
              let s = pathArray;
              if (s[s.length - 1] === "") {
                /* Last item being blank means that it is a folder,
                 * as folders are "like/this/", while files are
                 * "like/this/path"
                 * This check works with files that have extensions
                 * and those that do not.
                 */
                return;
              }
              let link = fileName.split("/").join(" » ");
              if (link.indexOf("»") < 0
                  || link.startsWith("assets")) {
                /* Don't list items in the root directory
                 * or page elements (CSS, images, etc.).
                 * This won't prevent unauthorized access,
                 * but it does help reduce clutter in the
                 * listings.
                 */
                return;
              }
              createFileListing(fileName, url, fileCount);
              fileCount++;
            }
          })
        })
      })
    })
  });
}

function setAccordionWatchers() {
  /* This method sets up click event listeners for each file emebd so when you
   * click on an Embed link to preview it, it changes the chevron accordingly,
   * and changes it back when you close the embed.
   */
  $(document).on('click', '.accordion-row-toggler', function() {
    /* This will bind the click event to any new .accordion-row-toggler
     * elements added after this method executes.
     * Credit for this goes to: https://stackoverflow.com/a/11961219
     * More info: https://stackoverflow.com/a/1207393
     */
    if ($(this).has('i')) {
      if ($(this).find('i:first')[0].classList.contains('fa-chevron-circle-down')) {
        $(this).find('i:first')[0].classList.replace('fa-chevron-circle-down', 'fa-chevron-circle-up');
      } else {
        $(this).find('i:first')[0].classList.replace('fa-chevron-circle-up', 'fa-chevron-circle-down');
      }
    }
  });
}

function setFolderWatchers() {
  /* This method sets up click event listeners for each folder element so when you
   * click on a folder to open it, it changes the icon to an open folder icon,
   * and changes it back when you close the folder.
   */
  $(document).on('click', '.folder-list-entry', function() {
    /* This will bind the onClick to any new .folder-list-entry elements added
     * after this method executes.
     * Credit for this goes to: https://stackoverflow.com/a/11961219
     * More info: https://stackoverflow.com/a/1207393
     */
    if ($(this).has('i')) {
      if ($(this).find('i:first')[0].classList.contains('fa-folder-open')) {
        $(this).find('i:first')[0].classList.replace('fa-folder-open', 'fa-folder');
      } else {
        $(this).find('i:first')[0].classList.replace('fa-folder', 'fa-folder-open');
      }
    }
  });
}

function disableVoidLinks() {
  /* This method just makes it so links I intend to act as buttons will not
   * append a "#" to the URL when clicked.
   */
  $(document).on('click', 'a.void', function() {
    return false;
  });
}

function displayError(messageHTML) {
  let template = '<div class="alert alert-danger alert-dismissible fade show" role="alert"><h4 class="alert-heading">An error occurred:</h4><span id="error-message-alert">{0}</span><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
  let alertHTML = template.split('{0}').join(messageHTML);
  $('#alerts').html(alertHTML);
}

$(window).on('load', function() {
  // Fetch config file
  $.ajax({
    type: 'get',
    url: 'assets/js/config.json',
    timeout: 10000,
    error: function(xhr, status, error) {
      document.getElementById('files').innerHTML = "Error: Unable to fetch config file.";
    },
    dataType: 'json',
    success: function(data) {
      let region = data.region;
      let bucket = data.bucket;
      let startingPosition = data.startingPosition;
      fileList(region, bucket, startingPosition);
    }
  });
  setFolderWatchers();
  setAccordionWatchers();
  disableVoidLinks();
  try {
    galleryInitialSetup();
    /* This is in the gallery.js file, so just incase the browser executes this
     * before it has loaded, I have this AJAX call to load the script.
     */
  } catch (ReferenceError) {
    $.getScript('assets/js/gallery.js')
      .done(function(script, status) {
        galleryInitialSetup();
        console.log('Gallery script had to be manually loaded before calling setup function');
      })
      .fail(function(jqxhr, settings, exception) {
        displayError('Unexpected error loading gallery plugin. Image gallery will not work.');
        console.log('Error: Unable to load gallery.js file! Is the link correct?');
      });
  }
  try {
    getFileIcon('txt');
    /* This is in the fileicons.js file, so just incase the browser executes this
     * before it has loaded, I have this AJAX call to load the script.
     */
  } catch (ReferenceError) {
    $.getScript('assets/js/fileicons.js')
      .done(function(script, status) {
        getFileIcon('txt');
        console.log('File icons script had to be manually loaded before calling setup function');
      })
      .fail(function(jqxhr, settings, exception) {
        displayError('Unexpected error loading file icons file. File and folder icons will not work.');
        console.log('Error: Unable to load fileicons.js file! Is the link correct?');
      });
  }

  let lightToggles = 0;
  let logoToggles = 0;

  /*$('.navbar-toggler-icon').on('click', function() {
    changeLogoOnDropdown(logoToggles);
    logoToggles++;
  });*/
  /* If you have a logo that is layered on top of the navbar,
   * uncomment this so you can have the logo change when a user
   * opens the dropdown menu so that the overhanging logo will
   * not be on top of the text in the menu.
   */
  
  let lightTheme = document.getElementById('light-mode-link');
  lightTheme.onclick = function() {
    lightMode(lightToggles);
    lightToggles++;
  };
  
  let isIE = detectIE();
  
  if (!isIE) {
    /* I used to have it be if (isIE) and add in the IE message,
     * but IE has a script error that causes it to terminate the
     * script before it got to this part, so I had to make it
     * displayed by default and removed if the user isn't running
     * IE. Not ideal since people with JS see the message, but
     * it's the easiest way of doing this.
     */
    $('#ie-message').html('');
  }
  $('#js-message').html('');
  
  setTimeout(function(){
    /* Without the timeout, the list elements have their tags reset
     * for some reason, but the delay prevents that.
     */
    if (getCookie('lightMode')) {
      lightMode(lightToggles);
      lightToggles++;
    }
  }, 2000);
});
